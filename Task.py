from random import randint
import matplotlib.pyplot as plt


def generate_three_points():
    tpoints = [randint(1,10) for i in range (1,7)]
    x = tpoints[:2]
    y = tpoints[2:4]
    z = tpoints[4:]
    print 'Coordinates of x: {0},{1}'.format(x[0],x[1])
    print 'Coordinates of y: {0},{1}'.format(y[0],y[1])
    print 'Coordinates of z: {0},{1}'.format(z[0],z[1])
    return x,y,z

def is_on_straight_line(a,b,c):
    if (a[0]-c[0])*(b[1]-c[1])-(b[0]-c[0])*(a[0]-c[1])==0:
        return "Dots are on the same line"
    else:
        return "Dots are NOT on the same line"

def draw_a_plot(a,b,c):
    plt.title("Simple plot")
    plt.plot(0, 0)
    plt.plot(12, 12)
    plt.xlabel("this is X")
    plt.ylabel("this is Y")

    plt.plot([a[0],b[0]], [a[1],b[1]], marker="o", color="blue", linestyle="dashed")
    plt.plot(c[0],c[1], "o")
    plt.show()



points = generate_three_points()
print is_on_straight_line(*points)
draw_a_plot(*points)
